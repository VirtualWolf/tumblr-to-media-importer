/* eslint-disable no-console */
/* eslint-disable max-statements */

const request = require('superagent');
const moment = require('moment-timezone');
const htmlparser = require('htmlparser2');
const Entities = require('html-entities').AllHtmlEntities;
const Uri = require('jsuri');
const fsx = require('fs-extra');

const postType = process.env.POST_TYPE || 'photo';

function constructTumblrUrl() {
    return new Uri('https://api.tumblr.com')
        .setPath(`/v2/blog/virtualwolf.tumblr.com/posts/${postType}`)
        .addQueryParam('before', process.env.BEFORE_EPOCH || '1483228800') // 1st January 2017, 00:00 GMT
        .addQueryParam('api_key', process.env.CONSUMER_KEY);
}

let offset = 0;

async function getPosts() {
    try {
        const response = await request.get(
            constructTumblrUrl()
                .addQueryParam('offset', offset)
                .toString()
        );

        if (response.body.response.posts.length > 0) {
            for (const post of response.body.response.posts) {
                await parsePost(post);
            }

            offset = offset + 20;
            console.log(`\nGetting new batch starting at offset ${offset}`);
            await getPosts();
        }
    } catch (err) {
        console.log(err.message);
    }
}

async function parsePost(post) {
    if (postType === 'video' && post.video_type !== 'tumblr') {
        console.log(`Post ${post.id}  Skipping video due to non-Tumblr video type (got ${post.video_type}).`);
        return;
    }

    let caption;

    const parser = new htmlparser.Parser({
        ontext: text => {
            const entities = new Entities();
            caption = caption === undefined
                ? entities.decode(text)
                : caption + entities.decode(text);
        }
    });
    parser.write(post.caption);
    parser.parseComplete();

    try {
        const tags = postType === 'video'
            ? [...post.tags, 'video']
            : post.tags;

        const mediaPostId = await addPost({
            message: caption,
            created: moment.unix(post.timestamp).utc().tz('Australia/Sydney').format('YYYY-MM-DD HH:mm:ss'),
            tags,
            tumblr_post_id: post.id,
        });

        if (postType === 'video') {
            await addFilesToPost({
                fileType: 'video',
                mediaPostId,
                tumblr_post_id: post.id,
                url: post.video_url,
            });
        } else if (postType === 'photo') {
            await processImagesForPost({mediaPostId, post});
        }
    } catch (err) {
        console.log(err.message);
        return;
    }
}

async function addPost({message, created, tags, tumblr_post_id}) {
    console.log(`\nPost ${tumblr_post_id}  Adding post`);
    try {
        const response = await request.post(`${process.env.TARGET_URL}/rest/tumblr?p=${process.env.MEDIA_TOKEN}`)
            .send({
                message,
                created,
                tags,
                tumblr_post_id,
            });

        return response.body.id;
    } catch (err) {
        console.log(`Post ${tumblr_post_id}  Error adding post: ${err.response.res.text}`);
    }
}

async function processImagesForPost({mediaPostId, post}) {
    if (post.photos.length > 1) {
        for (const photo of post.photos) {
            await addFilesToPost({
                fileType: 'photo',
                mediaPostId,
                tumblr_post_id: post.id,
                url: photo.original_size.url,
            });
        }
    } else {
        await addFilesToPost({
            fileType: 'photo',
            mediaPostId,
            tumblr_post_id: post.id,
            url: post.photos[0].original_size.url,
        });
    }
}

async function addFilesToPost({fileType, mediaPostId, tumblr_post_id, url}) {
    console.log(`Post ${tumblr_post_id}  Adding file ${url} (${fileType})`);

    const check = await request.post(`${process.env.TARGET_URL}/rest/tumblr/isMediaAlreadyImported?p=${process.env.MEDIA_TOKEN}`)
        .send({
            original_url: url
        });

    if (check.body === true) {
        return;
    }

    const req = await request.get(url);

    let filename;

    if (fileType === 'photo') {
        if (req.type === 'image/jpeg') {
            filename = 'file.jpg';
        } else if (req.type === 'image/png') {
            filename = 'file.png';
        } else if (req.type === 'image/gif') {
            filename = 'file.gif';
        } else {
            console.log(`Post ${tumblr_post_id}  Unrecognised filetype for ${url}`);
            return;
        }
    } else if (fileType === 'video') {
        filename = 'file.mp4';
    }

    if (filename === null) {
        return;
    }

    await fsx.outputFile(filename, req.body);

    try {
        await request.post(`${process.env.TARGET_URL}/rest/tumblr/${mediaPostId}?p=${process.env.MEDIA_TOKEN}`)
            .field('tumblr_post_id', tumblr_post_id)
            .field('original_url', url)
            .attach('file', filename);
    } catch (err) {
        console.log(`Post ${tumblr_post_id}  Error adding file ${url}: ${err}`);
    }

}

(async function main() {
    await getPosts();
})();